<?php  defined('C5_EXECUTE') or die("Access Denied."); ?>
<div class="circle-icon-wrap">
  <?php if ($linkURL) { ?>
    <a href="<?php echo $linkURL?>">
  <?php } ?>
      <div class="circle-icon">
        <i class="fa fa-<?php echo $icon?>"></i>
      </div>
  <?php if ($linkURL) { ?>
    </a>
  <?php } ?>
  <?php if ($title) { ?>
    <h3> <?php echo $title?></h3>
  <?php } ?>
  <?php
  if ($paragraph) {
      echo $paragraph;
  }
  ?>
</div>
