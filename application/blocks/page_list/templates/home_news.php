<?php
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$c = Page::getCurrentPage();
$dh = Core::make('helper/date'); /* @var $dh \Concrete\Core\Localization\Service\Date */
?>
<?php if ($c->isEditMode() && $controller->isBlockEmpty()) {
    ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.')?></div>
<?php
} else {
    ?>
<div class="news-wrap">
    <?php if (isset($pageListTitle) && $pageListTitle): ?>
    <h1><?php echo h($pageListTitle) ?></h1>
    <?php endif; ?>
    <dl class="news-list">
    <?php foreach ($pages as $page):
    $title = $th->entities($page->getCollectionName());
    $url = ($page->getCollectionPointerExternalLink() != '') ? $page->getCollectionPointerExternalLink() : $nh->getLinkToCollection($page);
    $target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
    $target = empty($target) ? '_self' : $target;
    $date = $dh->formatDate($page->getCollectionDatePublic(), true);
    ?>
        <dt><?php echo $date?></dt>
        <dd><a href="<?php echo $url ?>" target="<?php echo $target ?>"><?php echo $title ?></a></dd>
	<?php endforeach; ?>
    </dl>
</div>
<?php
} ?>