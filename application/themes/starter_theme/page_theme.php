<?php
namespace Application\Theme\StarterTheme;
use Concrete\Core\Page\Theme\Theme;
class PageTheme extends Theme
{
  
    public function getThemeName()
    {
        return t('Starter theme'); // テーマ名
    }

    public function getThemeDescription()
    {
        return t('Sample theme'); // テーマの説明
    }

    protected $pThemeGridFrameworkHandle = 'bootstrap3';

    public function registerAssets()
    {
        $this->providesAsset('javascript', 'bootstrap/*');
        $this->providesAsset('css', 'bootstrap/*');
        $this->providesAsset('css', 'core/frontend/*');
        $this->providesAsset('css', 'blocks/page_list');
        $this->providesAsset('css', 'blocks/feature');
        $this->providesAsset('css', 'blocks/social_links');

        $this->requireAsset('css', 'font-awesome');
        $this->requireAsset('javascript', 'jquery');
        $this->requireAsset('javascript', 'picturefill');
        $this->requireAsset('javascript-conditional', 'html5-shiv');
        $this->requireAsset('javascript-conditional', 'respond');
    }

    public function getThemeEditorClasses()
    {
        return [
            ['title' => t('Span'), 'spanClass' => '', 'forceBlock' => '-1'],
            ['title' => t('Standard Button'), 'spanClass' => 'btn btn-default', 'forceBlock' => '-1'],
            ['title' => t('Success Button'), 'spanClass' => 'btn btn-success', 'forceBlock' => '-1'],
            ['title' => t('Primary Button'), 'spanClass' => 'btn btn-primary', 'forceBlock' => '-1'],
        ];
    }
}