<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<footer class="footer-wrap">
    <div class="container">
        <?php
        $a = new GlobalArea('Footer Navigation');
        $a->display();
        ?>
        <div class="copy">&copy;2018.</div>
    </div>
</footer>

</div>
<script src="<?php echo $view->getThemePath()?>/js/bootstrap.min.js"></script>
<?php View::element('footer_required'); ?>

</body>
</html>