<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<!DOCTYPE html>
<html lang="<?php echo Localization::activeLanguage()?>">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php View::element('header_required');?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/earlyaccess/mplus1p.css">
    <link rel="stylesheet" href="<?php echo $view->getThemePath()?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $view->getThemePath()?>/css/main.css">
</head>
<body>

<div class="<?php echo $c->getPageWrapperClass()?>">

<header class="header-wrap">
    <div class="container">
        <div class="row">
            <div class="logo-area">
                <?php 
                $a = new GlobalArea('Header Site Title');
                $a->display();
                ?>
            </div>
            <div class="header-links-area">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#gloval-navbar-1">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="gloval-navbar-1" class="navbar-collapse collapse">
                    <?php
                    $a = new GlobalArea('Header Navigation');
                    $a->display();
                    ?>
                </div>
            </div>
        </div>
    </div>
</header>