<?php

/**
 * -----------------------------------------------------------------------------
 * Generated 2018-08-13T20:32:15+09:00
 *
 * DO NOT EDIT THIS FILE DIRECTLY
 *
 * @item      misc.do_page_reindex_check
 * @group     concrete
 * @namespace null
 * -----------------------------------------------------------------------------
 */
return [
    'locale' => 'ja_JP',
    'version_installed' => '8.4.2',
    'version_db_installed' => '20180716000000',
    'misc' => [
        'login_redirect' => 'DESKTOP',
        'access_entity_updated' => 1533992804,
        'do_page_reindex_check' => false,
    ],
];
