<?php

/**
 * -----------------------------------------------------------------------------
 * Generated 2018-08-11T22:05:53+09:00
 *
 * DO NOT EDIT THIS FILE DIRECTLY
 *
 * @item      sites.default.name
 * @group     site
 * @namespace null
 * -----------------------------------------------------------------------------
 */
return [
    'sites' => [
        'default' => [
            'name' => 'tre_sample',
        ],
    ],
];
